<?php

namespace Pointspay\Pointspay\Test\Model\Method\FakeBuilder;

use Magento\Payment\Gateway\Request\BuilderInterface;
use PHPUnit\Framework\TestCase;

class PublicKeyDataBuilder extends \Pointspay\Pointspay\Gateway\Request\PublicKeyDataBuilder
{

    public function build(array $buildSubject) :array
    {
        $request['clientConfig']['key_info']['public_key'] = '-----BEGIN CERTIFICATE-----
MIIJazCCBVOgAwIBAgIUYLuWZDsBh1HfptsO7xqoBxq/wN8wDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yNDAyMTYwOTM3MzlaFw0yNzAy
MTUwOTM3MzlaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggQiMA0GCSqGSIb3DQEB
AQUAA4IEDwAwggQKAoIEAQC/M43iujZ/ipJshz3yXiSADyChMRv/X/1nICs5kpGH
x3LR/strl/HrKL1tCq21rb+EbGndXe1YT0Tq+hQW3EasgF5usG1PV4OYCHRIgsDX
e2ikUsajrW5EjNKZ595jnzYe8yWO9Pg+kdHmJKw6eElYceJVJ3IwwWlPNmUG+xAz
wvcvcZJIwg3XDjdjJGNC1qnV3aSkQoeWqsMp32kqF0l+Kxl6uM9IctPMDg3i1YrC
Xnro2zHU03KI9qXDI2DU4TJj7Iltxr3vmhM0zTxpVo0djF/+jP6uW9fHVQRMsozL
51YBHXUvcM5l6XizCgb1AcQ7VtYY59N5PLHjVWct+6oV92ZRwcNxz7A4CGE/hCpu
+8KK+I7pWyVOA1pR5uQtDDrwN8qPF33Zg9WGKXbbCM3ow9tTQkXOdDeJKDK5AFDz
fn9mI19DJTcfJUyceTAA/nn9U20GNnQr8VaxgT+8tq8DAcU/BFRjXcFnUBDBOKQI
vXNE/LR/sALwm55irghWkI8j1t7mYooTyCxfjNVRj+VSPXYN7VIyHZ/E4q18iz3s
iXZpcORTWXOttu8z58meO/28HrelEUSpamSfyIvjtyLer9F7gO5I1KzVKIX5iM2G
sjENAf04sOhGnrdZ6t5zk61OyFyF4OjLHJQUs+kjJLQGrDYaH95xHv+zOM1K9lv/
GkZI2s0n9P/swiQgXKlzjHf7/eXAW6++Ctmymyh/f4L6nRJCy+NZ35ZrzT+ZFYD/
/j9nbezV758ZlVwnSTS7La6LO9kcE8mmCTgLIxPkoYllLSPreQcKbhIk6hZyPTu8
OwKLNMGiJtiDggxgjKtjcv87aH4ZNlO9U8h0hFzwW3lb+qDmPQGai3kuskZUrpPG
ycID6UcbLVaDRgXpEu+HYPa/B3vPSi0JY+TnJiykp9etdf7JOstbbr8mZVraWpnR
sdmgTo3WbxUkZz/Lf1mmLWl94A+ir47BbGQGktI7rSgGw71JCJu8sgzB121TBH3r
yY9SeDoUuYZ7bVSbh0rq0cy2FiNhOv3dfH0WUNeYL90K55g76TM0mtiE5Ds641RF
Q8sHIs6A5krDJkXDR5si9KL6hsmQT715EDvWsdMtc0GlNpwrBQIlm6k3hWsyFHPz
OktqLoMYVmbVF3B2DgiLawoubioLA+otNKz/G4dp23zIES7DWPexF1btT2D60Hl9
m8GWleVMM1oXtA4fQGpRTq2z/+QCkBLkFbQj1cqwYSthRLi6BFhtXTffejUkv6yK
22PBimStVmFDWynjkh6uRNhzErHwGpfwv4rRhMZhMLjC9D1B99/Z34xv9dib/lMQ
X07OoUd7qecrb/TLYz1nsDqEncW4ewMCKD7uHtHXlxhPAgMBAAGjUzBRMB0GA1Ud
DgQWBBRByYVWHhs1zS7BXvHSAAembB5VfTAfBgNVHSMEGDAWgBRByYVWHhs1zS7B
XvHSAAembB5VfTAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IEAQCx
tdOPYjE49s2TVpw1P+hEaxPX9K1Wftjqg0wjJkBc2BZjnhb0XcUG45lSX6vAvEHl
2kMv1PqpxEZ4i6vrxd8yEDGOyVgL3Js+AwArZuOGsVe6zZO64SudrHbYy/FAPQOg
uGXsoDhbGolf2MmijvcgnjXRXEPxV5f2UFhGZtTx4dASuHDg0ojLpiT6Ild0eSWC
28S9w94fU2w3nFopRz3gQnJm4ST67XoACngLHsM95CJUQ6gmbJNMYMWj7giEL18W
frwDu8i9xVUHcB/sIvhdxZ2I6Ef+/HTFYMDxc22n33fBT5fp/actehfnYseEdu5f
rgRoOH/mFeVlB+5ZzAEbjh7Xypm2D0GSOt+XM7Eakeo46LCmB0IbssInAGeNRC8w
XjrHoeW9pYph/jOUwC2+uG/A7z73ns7IwZ7Zgl8bc9LpkyQqwdjzH2jk6NRRHsdy
oAcr9QybVgSfhYuNsUyAUJ51VG1+loCnTUEsQUKMmYjMtLQz7mf303mvjBB8JnVi
3OGCxyh7LrPNppkOT+y+3b5aA4KEaYiuFrhBv1FXfqix3O8Yc65fJtnTCeOdi5Hb
M+96elDJXV30jxVlmkc311P6KN+/dsycHgpADDE+o+80k8FIkV06uaVvRaRpVIU1
2JCmx2p7c+606RVU8atSNAJVFTzHqrGD2IgXf+Gs9rKStby/GJOtyCKR5Jq6njWh
8XCb+UewayCVg2X5QOQIil4Sycoz5r2NOn2N31/jygRewfylno8OQgoyEg0njqve
lvpMkmqULdYcFS8XaFCbMgQQpjUPd4kqViORsmv9C87VdTf/TFQdl004YkoitoZE
EiPUb+VpOhwDvG0U1jvPPB1M8hx0HpFuFG6OMZuEqguOEUM+URKb8ZpyO56pwT7P
Zryx2wlc+JJej37xNEA58IA8VI+zLTb5SUw23YqCjLupS+R/tMwW1bieFICNPktG
cCXaZDU2On/AugXQ0GYOQAI1mImLlapU9e9BjRbwHTvWu1/B2elW8hgNPICXT6Jq
T4TsT6Sn+kuafU7vVH4tllfzgKWouoxwfM0lGxPQ/IItb002QPMFU9sXYVb8LLnw
D4AbXVILUAbG9ZnMS/Grk+0Nlp2ACeVQF3H10p5inCp8BZ2Cl7VN9V1J2MKuMTXN
ZEUmjc6PtLvami91s8QlDKR/JCau9iTp0Y/4/ldXfOXQoKkpyeX7GVSiZ5OGIKMW
NTYR9PYe+3XisCLYVfPJASCP0cV/2v7vdVWJGnxR82rribP5rrB9/QSJa0p+bQue
HHZOFXgJoo9X+LyRg5Jw+BlA8hXK2JVJgzKg9fpV84j0OuZs+O3L9J12Bv+P9IU7
a3QmLVA7z7jdBHwhj+Uo
-----END CERTIFICATE-----
';
        return $request;
    }
}
